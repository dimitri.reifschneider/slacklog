type Decision = {
  id: number;
  text: string;
  createdBy: string;
  createdAt: Date;
};

export const getServerSideProps = async () => {
  const createdAt = new Date().toDateString();
  const decisions = [
    {
      id: 1,
      text: "dasfghs",
      createdBy: "Paul",
      createdAt,
    },
    {
      id: 1,
      text: "dasfghs",
      createdBy: "Paul",
      createdAt,
    },
  ];

  return {
    props: {
      decisions,
    },
  };
};

export default ({ decisions }: { decisions: Decision[] }) => (
  <ul>
    {decisions.map(({ text }, index) => (
      <li key={`decision-${index}`}>{text}</li>
    ))}
  </ul>
);
